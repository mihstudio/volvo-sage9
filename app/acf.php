<?php

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' => 'Theme Options',
        'menu_title' => 'Theme Options',
        'menu_slug' => 'theme-options',
        'capability' => 'edit_posts',
        'position' => 40,
        'redirect' => false
    ));

}

function my_acf_google_map_api($api)
{
    $api['key'] = 'AIzaSyC7lC738CtpQbqbJMsqOEt0FwSAyUK5UWk';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

