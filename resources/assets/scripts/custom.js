$(document).ready(function () {
  $('.authorizations__logos').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 1000,
    slidesToShow: 6,
    centerMode: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });

  $('.blog__items').slick({
    dots: true,
    infinite: false,
    slidesToShow: 3,
    arrows: false,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });

  $('.home-slider').slick({
    dots: false,
    slidesToShow: 1,
    arrows: true,
    infinite: true,
  });
  var theLanguage = $('html').attr('lang');

  if (theLanguage == 'pl-PL') {
    $('.wpcf7-select').select2({
      minimumResultsForSearch: -1,
      placeholder: {
        id: '', // the value of the option
        text: 'Wybierz dział obsługi',
      },
      width: '100%',
      dropdownParent: $('.menu-189'),
    });
  } else {
    $('.wpcf7-select').select2({
      minimumResultsForSearch: -1,
      placeholder: {
        id: '', // the value of the option
        text: 'Select a service department',
      },
      width: '100%',
      dropdownParent: $('.menu-189'),
    });
  }

  $('.show-menu').on('click', function (e) {
    e.preventDefault();

    if ($(this).parent('a').attr('aria-expanded') === 'false') {
      $(this).parent('a').attr('aria-expanded', 'true');
    } else {
      $(this).parent('a').attr('aria-expanded', 'false');
      $(this).parent('a').next('.dropdown-menu').toggleClass('d-none');
    }
  });


  $('ul.page-numbers li span').each(function () {
    const $this = $(this);
    if ($this.hasClass('current')) $this.parent('li').addClass('active');
  });
});

/*----- fixed icon on desktop -----*/

var btn = document.querySelector('.btn.phone');
var actions = document.querySelector('.actions')

if (btn) {
  btn.addEventListener('click', () => {
    if (actions.classList.contains('active')) {
      btn.classList.remove('active')
      actions.classList.remove('active')
    } else {
      btn.classList.add('active')
      actions.classList.add('active')
    }
  });
}

/*----- Tooltip on fixed icon -----*/

window.onload = function () {
  var tooltip = document.querySelector('.contact-tooltip');

  function showTooltip() {
    tooltip.classList.add('is-visible');
    document.cookie = 'hasSeenTooltip=true; max-age=3600;';
    clearTimeout();
    setTimeout(closeTooltip, 5000);
  }

  setTimeout(showTooltip, 4000);

  // Close the tooltip after 5 seconds
  function closeTooltip() {
    tooltip.style.opacity = 0;
    clearTimeout();
    setTimeout(setDisplayNone, 500)
  }

  // Set display none after the opacity has been set to zero for nice transition
  function setDisplayNone() {
    tooltip.style.display = 'none';
    tooltip.classList.remove('is-visible');
  }
};

const hamburger = document.querySelector('#hamburger');
const mobile_menu = document.querySelector('.navbar');
const body = document.querySelector('body');
const topbar = document.querySelector('.topbar');

const handleClick = () => {
  hamburger.classList.toggle('header__hamburger--is-active');
  mobile_menu.classList.toggle('is-active');
  body.classList.toggle('scroll-lock');
  topbar.classList.toggle('topbar--dark');
}

hamburger.addEventListener('click', handleClick);

document.addEventListener('click', (evt) => {
  const flyoutElement = document.querySelector('.navbar');
  const header = document.querySelector('.topbar');
  let targetElement = evt.target; // clicked element

  do {
    if (targetElement == flyoutElement || targetElement == header) {
      // This is a click inside. Do nothing, just return.
      return;
    }
    // Go up the DOM
    targetElement = targetElement.parentNode;
  } while (targetElement);

  // This is a click outside.
  hamburger.classList.remove('header__hamburger--is-active');
  mobile_menu.classList.remove('is-active');
  body.classList.remove('scroll-lock');
  topbar.classList.remove('topbar--dark');
});

const search_icon = document.querySelector('.search-icon');
const search_form = document.querySelector('.search-form');
const header_btn = document.querySelector('.btn--header');

const handleClickSearch = () => {
  search_form.classList.toggle('is-active');
  header_btn.classList.toggle('is-active');
}

search_icon.addEventListener('click', handleClickSearch);


const toTop = $('#btn-top');

$(window).scroll(function () {
  if ($(window).scrollTop() > 300) {
    toTop.addClass('show');
  } else {
    toTop.removeClass('show');
  }
});

toTop.on('click', function (e) {
  e.preventDefault();
  $('html, body').animate({scrollTop: 0}, '300');
});

$('.card-body__btn .btn').on('click', function () {
  $(this).next('.hidden-form').slideToggle();
});
