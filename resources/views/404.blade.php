@extends('layouts.app')

@section('content-full')
  <section class="hero contract lazy " data-bg="<?= get_site_url(); ?>/wp-content/uploads/2021/01/hero-contracts.jpg">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="hero__center">
            <h1 class="hero__title">404</h1>
            <div class="breadcrump">
              @if ( function_exists('yoast_breadcrumb') )
                @php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); @endphp
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @if (!have_posts())
    <div class="container py-5 my-5">
      <div class="row">
        <div class="col-12 justify-content-center text-center">
          <div class="alert alert-warning">
            @php _e('Przepraszamy, ale strona, którą próbowałeś wyświetlić nie istnieje.', 'sage') @endphp
          </div>
          <div class="error__btn mt-5">
            <a href="<?= get_site_url(); ?>" class="btn btn-blue">@php _e('wróć do strony głównej', 'sage') @endphp</a>
          </div>
        </div>
      </div>
    </div>

  @endif
@endsection
