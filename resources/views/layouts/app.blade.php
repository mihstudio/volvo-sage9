<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
  <a id="top"></a>
    @php do_action('get_header') @endphp
    @include('partials.header')
    @include('partials.nav')
    @yield('content-full')
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
