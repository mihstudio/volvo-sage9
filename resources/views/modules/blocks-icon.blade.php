@php $color = get_sub_field('color_section'); @endphp

<section class="block-icon" @if($color) style="background-color: {{ $color }}" @endif>
  <div class="container">
    <div class="row">
      @if( have_rows('block-icon') )
        @while( have_rows('block-icon') ) @php the_row() @endphp
        @php
          $icon = get_sub_field('image');
          $title = get_sub_field('title');
          $text = get_sub_field('text');
        @endphp
        <div class="col-12 col-md col-lg">
          <div class="block-icon__single">
            {!! wp_get_attachment_image( $icon, 'full', "", array( "class" => "block-icon__img" ) ) !!}
            <div class="block-icon__content">
              <h3 class="block-icon__title">{{ $title }}</h3>
              <div class="block-icon__description">{!! $text !!}</div>
            </div>
          </div>
        </div>
        @endwhile
      @endif
    </div>
  </div>
</section>

