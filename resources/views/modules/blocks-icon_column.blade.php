@php
  $custom = get_sub_field('custom_section');
@endphp

<section class="block-icon @if($custom === true) {{ 'custom-icons' }} @endif">
  <div class="container">
    <div class="row">
      @if( have_rows('block_icon_columns') )
        @while( have_rows('block_icon_columns') ) @php the_row() @endphp
        @php
          $icon = get_sub_field('image');
          $title = get_sub_field('title');
          $text = get_sub_field('text');
        @endphp
        <div class="col-12 col-md col-lg">
          <div class="icon-column">
            <div class="icon-column__image">
              <img class="icon-column__img" src="{{ $icon['url'] }}" alt="{{ $icon['alt'] }}"/>
            </div>
            <h3 class="icon-column__title title-section centerLine">{!! $title !!}</h3>
            <div class="icon-column__description">{!! $text !!}</div>
          </div>
        </div>
        @endwhile
      @endif
    </div>
  </div>
</section>

