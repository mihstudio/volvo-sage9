@php
  $title = get_sub_field('title');
  $i = 1;
@endphp

<section class="career">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="title-section centerLine">
          {{ $title }}
        </h2>
      </div>
      <div class="col-12">


        <div class="accordion" id="accordionCareer">
          @if( have_rows('jobs') )
            @while ( have_rows('jobs') ) @php the_row(); @endphp
            @php
              $name = get_sub_field('name');
              $date_start = get_sub_field('date_start');
              $date_end = get_sub_field('date_end');
              $text = get_sub_field('text');
              $email_send = get_sub_field('email_send');
              $cv = get_sub_field('form_cv');
            @endphp
            <div class="card">
              <div class="card-header" id="heading-{{ $i }}">
                <h2 class="mb-0">
                  <button class="btn btn-card collapsed" type="button" data-toggle="collapse"
                          data-target="#collapse-{{ $i }}" aria-expanded="false" aria-controls="collapse-{{ $i }}">
                    <span class="name"> {{ $name }} </span>
                    <p class="btn-card__icon">
                      <span class="material-icons">keyboard_arrow_down</span>
                    </p>
                  </button>
                </h2>
              </div>

              <div id="collapse-{{ $i }}" class="collapse" aria-labelledby="heading-{{ $i }}"
                   data-parent="#accordionCareer">
                <div class="card-body">

                  @if( have_rows('description') )
                    @while ( have_rows('description') ) @php the_row(); @endphp

                    @if( get_row_layout() === 'text' )
                      @php $desc = get_sub_field('desc'); @endphp
                      {!! $desc !!}

                    @elseif( get_row_layout() === 'lists' )
                      @php $title = get_sub_field('title');
                        $list = get_sub_field('list'); @endphp

                      <div class="box-50">
                        <h3>{{ $title }}</h3>
                        {!! $list !!}
                      </div>

                    @endif
                    @endwhile
                  @endif
                  <div class="card-body__btn">
                    <a href="#!" class="btn btn-blue">
                      @php _e('Aplikuj teraz', 'sage'); @endphp
                    </a>
                    <div class="hidden-form form">
                      {!! do_shortcode($cv) !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @php $i++; @endphp
            @endwhile
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
