@php
  $title = get_sub_field('title');
  $text = get_sub_field('text');
  $list = get_sub_field('list');
@endphp

<section class="offer-center">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="offer-center__content">
          <h2 class="title-section centerLine">
            {{ $title }}
          </h2>
          <div class="text-center">
            {!! $text !!}
          </div>
          @if($list)
            <div class="custom-list">
              {!! $list !!}
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
