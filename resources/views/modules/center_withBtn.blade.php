@php
  $text = get_sub_field('text');
  $link = get_sub_field('link');
@endphp

<section class="offer-centerBtn">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="offer-center__content">
          <div class="text-center">
            {!! $text !!}
          </div>
          @if($link)
            <div class="offer-center__btn">
              <a href="{{ $link['url'] }}" class="btn btn-blue-text">
                {{ $link['title'] }}
              </a>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
