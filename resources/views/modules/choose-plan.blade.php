<section class="plans">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="title-section centerLine">
          {{ get_sub_field('title_section') }}
        </h2>
        <div class="text-center">
          {!! get_sub_field('text') !!}
        </div>
      </div>
      @if( have_rows('boxs') )
        @while( have_rows('boxs') ) @php the_row() @endphp
        @php $img = get_sub_field('image');
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            $list = get_sub_field('list');
            $link = get_sub_field('link');
        @endphp
        <div class="col-12 col-lg-4 mb-5">
          <div class="single-plan">
            <div class="single-plan__photo">
              <img class="lazy" src="{{ $img['url'] }}" alt="{{ $img['alt'] }}">
            </div>
            <div class="single-plan__content">
              <div class="single-plan__content-box">
                <h3 class="single-plan__title">
                  {{ $title }}
                </h3>
                <div class="single-plan__text">
                  {!! $text !!}
                </div>
                <div class="single-plan__list custom-list">
                  {!! $list !!}
                </div>
              </div>
              @if($link)
                <div class="single-plan__link">
                  <a href="{{ $link['url'] }}" class="btn btn-blue">
                    {{ $link['title'] }}
                  </a>
                </div>
              @endif
            </div>
          </div>
        </div>
        @endwhile
      @else
      @endif
    </div>
  </div>
</section>
