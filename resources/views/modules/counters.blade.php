<section class="counters">
  <div class="container">
    <div class="row">
      @if( have_rows('counter') )
        @while( have_rows('counter') ) @php the_row() @endphp
        @php
          $number = get_sub_field('number');
          $title = get_sub_field('title');
        @endphp
        <div class="counter col-12 col-md col-lg-3">
          <p class="counter__number"><span class="countup">{{ $number }}</span></p>
          <p class="counter__title">{{ $title }}</p>
        </div>
        @endwhile
      @endif
    </div>
  </div>
</section>
