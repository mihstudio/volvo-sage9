@php
  $bg = get_sub_field('image');
@endphp

<section class="hero contract lazy @if(is_page('serwis-24h')) {{ 'page-service24' }} @endif"
         data-bg="{{ $bg['url'] }}}">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="hero__center">
          <h1 class="hero__title">{{ get_sub_field('title') }}</h1>
          <div class="breadcrump">
            @if ( function_exists('yoast_breadcrumb') )
              @php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); @endphp
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

