@php
  $bg = get_sub_field('image');
  $title = get_sub_field('title');
  $description = get_sub_field('test');
  $link = get_sub_field('link');
@endphp

<section class="hero custom-offer lazy" data-bg="{{ $bg['url'] }}}">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12">
        <div class="hero__content">
          <h1 class="hero__title">{{ $title }}</h1>
          @if($description)
            <div class="hero__description">{!! $description !!}</div>
          @endif
          @if($link)
            <div class="hero__link">
              <a href="{{ $link['url'] }}" class="btn btn-blue">
                {{ $link['title'] }}
              </a>
            </div>
          @endif
          <div class="breadcrump">
            @if ( function_exists('yoast_breadcrumb') )
              @php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); @endphp
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

