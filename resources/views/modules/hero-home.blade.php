

<div class="home-slider">
  @if( have_rows('slider') )
    @while( have_rows('slider') ) @php the_row() @endphp
    @php
      $bg = get_sub_field('image');
      $title = get_sub_field('title');
      $description = get_sub_field('description');
      $link = get_sub_field('link');
    @endphp
    <section class="hero home lazy" data-bg="{{ $bg['url'] }}}">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md col-lg-7">
            <div class="hero__content">
              <div class="hero__title">{!! $title  !!}</div>
              <div class="hero__description">{!! $description  !!}</div>
              @if($link)
                <div class="hero__link">
                  <a href="{{ $link['url'] }}" class="btn btn-blue">
                    {{ $link['title'] }}
                  </a>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </section>
    @endwhile
  @endif
</div>
