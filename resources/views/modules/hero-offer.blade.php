@php
  $bg = get_sub_field('image');
  $title = get_sub_field('title');
  $description = get_sub_field('test');
  $link = get_sub_field('link');
@endphp

<section class="hero home offer lazy" data-bg="{{ $bg['url'] }}}">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="hero__content">
          <h1 class="hero__title">{{ $title }}</h1>
          <div class="hero__description">{!! $description !!}</div>
          @if($link)
            <div class="hero__link">
              <a href="{{ $link['url'] }}" class="btn btn-blue">
                {{ $link['title'] }}
              </a>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>

