@php
$title = get_sub_field('title');
$description = get_sub_field('description');
@endphp

<section class="authorizations">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if($title)
        <h2 class="heading heading--extra heading--center">
          {{ $title }}
        </h2>
        @endif
        @if($description)
        <p class="authorizations__description">{!! $description !!}</p>
        @endif
        @php $logos = get_sub_field('logos'); @endphp

        @if($logos)
          <div class="authorizations__logos">
            @foreach($logos as $logo)
              <div class="authorizations__logo">
                <img class="authorizations__img" src="{{ $logo['url'] }}" alt="{{ $logo['alt'] }}">
              </div>
            @endforeach
          </div>
        @endif
      </div>
    </div>
  </div>
</section>
