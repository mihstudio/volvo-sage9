<section id="ciekawostki" class="blog">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="heading heading--extra heading--center">
          {{ get_sub_field('title') }}
        </h2>
        @php
          $id = get_sub_field('category');

          $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'DESC',
            'category__in' => $id,
          );

          $query = new WP_Query( $args );
        @endphp
        @if($query->have_posts())
          <div class="blog__items">
            @while($query->have_posts())
              @php $query->the_post() @endphp
              <div class="blog__item">
                <div class="blog__image">
                  <img class="blog__img" src="{{ get_the_post_thumbnail_url() }}" alt="{{ get_the_title() }}">
                </div>
                <h3 class="blog__title">{{ get_the_title() }}</h3>
                <p class="blog__description">{{ get_the_excerpt() }}</p>
                <div class="blog__link">
                  <a class="blog__button" href="{{ get_permalink() }}"><?php _e('Czytaj więcej', 'sage'); ?> <span class="material-icons">keyboard_arrow_right</span></a>
                </div>
              </div>
            @endwhile
          </div>
        @endif

        @php wp_reset_postdata(); @endphp
      </div>
    </div>
  </div>
</section>
