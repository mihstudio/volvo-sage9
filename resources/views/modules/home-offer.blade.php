<section class="home-offer">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-6">
        <div class="main-block">
          @php $mainBlock = get_sub_field('mainBlock') @endphp
          <div class="main-block__image">
            <img class="main-block__img img-fluid" src="{{ $mainBlock['mainBlock_image']['url'] }}" alt="{{ $mainBlock['mainBlock_image']['alt'] }}" />
          </div>
          <div class="main-block__content">
            <h3 class="main-block__title">{{ $mainBlock['mainBlock_title'] }}</h3>
            <p class="main-block__description">{{ $mainBlock['mainBlock_description'] }}</p>
            <div class="main-block__link">
              <a href="{{ $mainBlock['mainBlock_link']['url'] }}" class="btn btn-blue">
                {{ $mainBlock['mainBlock_link']['title'] }}
              </a>
            </div>
          </div>
        </div>
        <div class="small-blocks">
          @if( have_rows('smallBlock') )
            <div class="row">
              @while( have_rows('smallBlock') ) @php the_row() @endphp
                @php
                  $smallBlockTitle = get_sub_field('title');
                  $smallBlockLink = get_sub_field('link');
                @endphp
                <div class="col-md-12 col-lg-12 col-xl-6">
                  <a href="{{ $smallBlockLink }}" class="small-block">
                    <h3>{{ $smallBlockTitle }}</h3>
                  </a>
                </div>
              @endwhile
            </div>
          @endif
        </div>
      </div>
      <div class="col-md-12 col-lg-6">
        <div class="mid-blocks">
          @if( have_rows('midBlock') )
            @while( have_rows('midBlock') ) @php the_row() @endphp
            @php
              $midBlockImage = get_sub_field('image');
              $midBlockTitle = get_sub_field('title');
              $midBlockDescription = get_sub_field('description');
              $midBlockLink = get_sub_field('link');
            @endphp

            <div class="mid-block">
              <div class="mid-block__image">
                <img class="mid-block__img img-fluid" src="{{ $midBlockImage['url'] }}" alt="{{ $midBlockImage['alt'] }}">
              </div>
              <div class="mid-block__content">
                <h3 class="mid-block__title">{{ $midBlockTitle }}</h3>
                <p class="mid-block__description">{{ $midBlockDescription }}</p>
                <div class="mid-block__link">
                  <a href="{{ $midBlockLink }}" class="btn btn-blue">
                    <span class="material-icons">keyboard_arrow_right</span>
                  </a>
                </div>
              </div>
            </div>
            @endwhile
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
