<section class="insurance">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12">
        <h2 class="insurance__title title-section centerLine">
          {{ get_sub_field('title') }}
        </h2>
      </div>
      <div class="col-12 col-md col-lg-10">
        <p class="insurance__description">{{ get_sub_field('text') }}</p>
      </div>
      @if( have_rows('blocks') )
        @while( have_rows('blocks') ) @php the_row() @endphp
        <div class="col-12 col-md col-lg-6">
          @php
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $desc = get_sub_field('text');
          @endphp
          <div class="insurance-block">
            <div class="insurance-block__image">
              <img class="insurance-block__img img-fluid" src="{{ $image['url'] }}" alt="{{ $image['alt'] }}" />
            </div>
            <div class="insurance-block__content">
              <h3 class="insurance-block__title">{{ $title }}</h3>
              <p class="insurance-block__description">{{ $desc }}</p>
            </div>
          </div>
        </div>
        @endwhile
      @endif
    </div>
  </div>
</section>
