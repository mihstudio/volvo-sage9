@php
  $image = get_sub_field('image');
  $title = get_sub_field('title');
  $text = get_sub_field('text');
  $color = get_sub_field('section_color');
  $class = get_sub_field('add_class');
@endphp

<section class="image-text {{ $class }}" @if($color) style="background-color: {{ $color }}" @endif>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-lg-7">
        <div class="image-text__photo">
          <img class="lazy" src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
        </div>
      </div>
      <div class="col-12 col-lg-5">
        <div class="image-text__content">
          <h2 class="image-text__title title-section topLine">{{ $title }}</h2>
          <div class="image-text__desc">
            {!! $text !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

