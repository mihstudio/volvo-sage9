@php
  $image = get_sub_field('image');
  $title = get_sub_field('title');
  $text = get_sub_field('text');
  $list = get_sub_field('list');
  $color = get_sub_field('section_color');
@endphp

<section class="text-image" @if($color) style="background-color: {{ $color }}" @endif>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-lg-6">
        <div class="text-image__content">
          <h2 class="text-image__title title-section topLine">{{ $title }}</h2>
          <div class="text-image__desc">
            {!! $text !!}
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="text-image__photo">
          <img class="lazy" src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
        </div>
      </div>
    </div>
  </div>
</section>

