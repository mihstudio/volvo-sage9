@php
  $title = get_sub_field('title');
  $text = get_sub_field('text');
  $list = get_sub_field('list');
  $color = get_sub_field('section_color');
@endphp

<section class="text-list" @if($color) style="background-color: {{ $color }}" @endif>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-lg-6">
        <div class="text-list__content">
          <h2 class="text-list__title title-section topLine">{{ $title }}</h2>
          <div class="text-list__desc">
            {!! $text !!}
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="text-list__desc custom-list">
          {!! $list !!}
        </div>
      </div>
    </div>
  </div>
</section>

