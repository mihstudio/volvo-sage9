@php
  $video = get_sub_field('video');
  $title = get_sub_field('title');
  $text = get_sub_field('text');
@endphp

<section class="video-text">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-lg-6">
        <div class="video-text__video">
            {!! $video !!}
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="video-text__content">
          <h2 class="video-text__title title-section topLine">{{ $title }}</h2>
          <div class="video-text__desc">
            {!! $text !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

