@php
$image = get_sub_field('image');
$title = get_sub_field('title');
$text = get_sub_field('text');
$form = get_sub_field('form');
@endphp

<section class="newsletter">
  <div class="container">
    <div class="row align-items-end">
      <div class="col-12 col-lg-6 order-2 order-lg-1">
        <h2 class="title-section ">{{ $title }}</h2>
        <div class="newsletter__desc">
          {!! $text !!}
        </div>
        <div class="newsletter__form">
          @php
           echo do_shortcode($form)
          @endphp
        </div>
      </div>
      <div class="col-12 col-lg-6 order-1 order-lg-2">
        <div class="newsletter__photo">
          <img class="newsletter__img" src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
        </div>
      </div>
    </div>
  </div>
</section>
