@php
  $image_different = get_sub_field('image_different');
  $image_sec = get_sub_field('image_sec');
  $title = get_sub_field('title');
  $list = get_sub_field('list');
  $list2 = get_sub_field('list2');
  $link = get_sub_field('link');
  $id = get_sub_field('id_section');
@endphp

<div class="col-12 col-md-6">
  <div id="{{ $id }}" class="different-offer__single single-offer">
    <h3 class="single-offer__title">
      {{ $title }}
    </h3>
    <div class="different-offer__list">
      <div class="custom-list @if($list2) {{ 'custom-list__col' }} @endif">{!! $list !!}</div>
      @if($list2)
        <div class="custom-list custom-list__col">{!! $list2 !!}</div>
      @endif
    </div>

    @if($image_different)
      <div class="different-offer__image">
        <img src="{{ $image_different['url'] }}" alt="{{ $image_different['alt'] }}">
      </div>
    @endif
    @if($image_sec)
      <div class="different-offer__image-sec">
        <img src="{{ $image_sec['url'] }}" alt="{{ $image_sec['alt'] }}">
      </div>
    @endif

    <div class="different-offer__btn right-side">
      @if($link)
        <a href="{{ $link['url'] }}" class="btn btn-blue">
          @if($link['title'] != '')
            {{ $link['title'] }}
          @else
            <span class="material-icons">arrow_forward_ios</span>
          @endif
        </a>
      @endif
    </div>
  </div>
</div>

