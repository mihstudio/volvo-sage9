@php
  $id = get_sub_field('id_section');
@endphp

<section id="{{ $id }}" class="offer-image home-offer">
  <div class="container">
    <div class="row">
      @if( have_rows('boxs') )
        @while( have_rows('boxs') ) @php the_row() @endphp
        @php
          $image = get_sub_field('image');
          $title = get_sub_field('title');
          $text = get_sub_field('text');
          $list = get_sub_field('list');
          $link = get_sub_field('link');
        @endphp
        <div class="col-12 col-md-6 mb-5">
          <div class="main-block">
            <div class="main-block__image">
              <img class="main-block__img img-fluid" src="{{ $image['url'] }}" alt="{{ $image['alt'] }}"/>
            </div>
            <div class="main-block__content">
              <div class="main-block__content-text">
                <h3 class="main-block__title">{{ $title }}</h3>
                <div class="main-block__description">{!! $text !!}</div>
                <div class="custom-list">{!! $list !!}</div>
              </div>
              @if($link)
                <div class="main-block__link @if($link['title'] === '') {{ 'right-side' }} @endif">
                  <a href="{{ $link['url'] }}" class="btn btn-blue">
                    @if($link['title'] != '')
                      {{ $link['title'] }}
                    @else
                      <span class="material-icons">arrow_forward_ios</span>
                    @endif
                  </a>
                </div>
              @endif
            </div>
          </div>
        </div>
        @endwhile

      @endif
    </div>
  </div>
</section>
