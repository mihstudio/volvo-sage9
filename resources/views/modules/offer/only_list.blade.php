@php
  $image = get_sub_field('image');
  $title = get_sub_field('title');
  $list = get_sub_field('list');
  $link = get_sub_field('link');
@endphp

<section class="list-row">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-6 pr-lg-0">
        <div class="single-offer">
          <h3 class="single-offer__title">
            {{ $title }}
          </h3>
          <div class="custom-list">{!! $list !!}</div>
          <div class="different-offer__btn right-side">
            @if($link)
              <a href="{{ $link['url'] }}" class="btn btn-blue">
                @if($link['title'] != '')
                  {{ $link['title'] }}
                @else
                  <span class="material-icons">arrow_forward_ios</span>
                @endif
              </a>
            @endif
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6 pl-lg-0">
        <div class="list-row__image">
          <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
        </div>
      </div>
    </div>

  </div>
</section>
