@php
  $image = get_sub_field('image');
  $title = get_sub_field('title');
  $text = get_sub_field('text');
  $list = get_sub_field('list');
  $link = get_sub_field('link');
@endphp

<section class="tires">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="single-offer">
          <div class="row">
            <div class="col-12 col-md-6 order-1">
              <h3 class="single-offer__title">
                {{ $title }}
              </h3>
              <div class="single-offer__text">{!! $text !!}</div>
              <div class="single-offer__list-col custom-list">{!! $list !!}</div>
            </div>
            <div class="col-12 col-md-6 order-2">
              <div class="single-offer__right-img">
                <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
                <a href="{{ $link['url'] }}" class="btn btn-blue">
                  {{ $link['title'] }}
                </a>
              </div>
            </div>
            @if( have_rows('box_text') )
              @while( have_rows('box_text') ) @php the_row() @endphp
              @php
                $title = get_sub_field('title');
                $text = get_sub_field('text');
              @endphp
              <div class="col-12 col-md-3 order-3">
                <h3 class="title-section bottomLine">
                  {{ $title }}
                </h3>
                <div class="single-offer__text">
                  {!! $text !!}
                </div>
              </div>
              @endwhile
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
