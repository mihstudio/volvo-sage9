<section id="contact" class="form">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md col-lg-8">
        <h2 class="heading heading--extra heading--center heading--white">
          {{ get_sub_field('title') }}
        </h2>
        {!! do_shortcode(get_sub_field('shortcode')) !!}
      </div>
    </div>
  </div>
</section>
