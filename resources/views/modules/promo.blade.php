<section class="curiosities">
  <div class="container">
    <div class="row">
      @if( have_rows('box') )
        @while ( have_rows('box') ) @php the_row(); @endphp
        @php
            $image = get_sub_field('img');
            $title = get_sub_field('title');
            $link = get_sub_field('link');
        @endphp
        <div class="col-12 col-md-6 col-lg-4 blog__item">
          <div class="blog__image">
            @if($image)
              <img class="blog__img" src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
            @endif
          </div>
          <h3 class="blog__title">
            {{ $title }}
          </h3>
          <div class="blog__link">
            <a class="blog__button" href="{{ $link['url'] }}">
              <?php _e( 'Zobacz więcej', 'sage' ); ?>
                <span class="material-icons">keyboard_arrow_right</span>
            </a>
          </div>
        </div>

        @endwhile
      @endif

    </div>
  </div>
</section>
