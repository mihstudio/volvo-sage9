<section class="page mt-5 pt-5">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="my-5 py-5">
          <h1>@php the_title() @endphp</h1>
          @php the_content() @endphp
          {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
        </div>
      </div>
    </div>
  </div>
</section>

