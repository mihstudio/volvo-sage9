@php
  $image = get_the_post_thumbnail_url(get_the_ID(),'medium');
@endphp
<div class="col-12 col-md-6 col-lg-4 blog__item">
  <div class="blog__image">
    @if($image)
      <img class="blog__img" src="{{ $image }}" alt="{{ get_the_title() }}">
    @else
      <img class="blog__img" src="@asset('images/placeholder.jpg')" alt="{{ get_the_title() }}">
    @endif
  </div>
  <h3 class="blog__title">
    <a class="" href="{{ get_permalink() }}">{{ get_the_title() }}</a>
  </h3>
  <p class="blog__description">{{ get_the_excerpt() }}</p>
  <div class="blog__link">
    <a class="blog__button" href="{{ get_permalink() }}"><?php _e('Czytaj więcej', 'sage'); ?> <span
        class="material-icons">keyboard_arrow_right</span></a>
  </div>
</div>
