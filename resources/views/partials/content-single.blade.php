@php
  $bg = get_the_post_thumbnail_url(get_the_ID(),'full');
@endphp

@if($bg)
  <section class="hero contract lazy" data-bg="{{ $bg }}">
    @else
      <section class="hero contract lazy" data-bg="@asset('images/hero-placeholder.jpg')">
        @endif
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="hero__center">
                <h1 class="hero__title">{{ get_the_title() }}</h1>
              </div>

            </div>
          </div>
        </div>
      </section>


      <section class="single-post">
        <div class="container">
          <div class="row">
            <div class="col-12">

              @php the_content() @endphp
            </div>
          </div>
        </div>
      </section>

