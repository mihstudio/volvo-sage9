<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <p class="footer__address">
          <strong>OMEGA Truck Center sp. z o.o.</strong><br/>
          {{ get_field('footer_address', 'option') }}
        </p>
        <p class="footer__hours">
          <?php _e('Copyright © Omega Truck Service |', 'sage') ?> {{ date('Y') }}
        </p>
        <div class="social-media">
          @if( have_rows('social_media', 'option') )
            @while( have_rows('social_media', 'option') ) @php the_row() @endphp
            @php
              $icon = get_sub_field('icon');
              $link = get_sub_field('link');
            @endphp
            <a href="{{ $link['url'] }}" target="{{ $link['target'] }}" class="social-media__link">
              {!! $icon !!}
            </a>
            @endwhile
          @endif
        </div>
      </div>
      <div class="col-lg-3">
        <div id="sidebar-primary" class="sidebar">
          <?php dynamic_sidebar( 'sidebar-1' ); ?>
        </div>
      </div>
      <div class="col-lg-3">
        <div id="sidebar-primary" class="sidebar">
          <?php dynamic_sidebar( 'sidebar-2' ); ?>
        </div>
      </div>

    </div>
  </div>
</footer>
<div class="contact-tooltip"><p><?php _e('Masz do nas pytanie? Chętnie na nie odpowiemy', 'sage') ?></p></div>
<div class="action">
  <button class="btn phone">
    <span class="material-icons icon close">close</span>
    <span class="material-icons icon phone">call</span>
  </button>
  <div class="actions"><a
      href="tel:{{ str_replace(' ','',get_field('fixed-icon_number', 'options')) }}">+48 {{ get_field('fixed-icon_number', 'options') }}</a>
  </div>
</div>
<a href="tel:{{ str_replace(' ','',get_field('fixed-icon_number', 'options')) }}" class="fixed-icon-mobile">
  <div class="contact-tooltip"><p><?php _e('Masz do nas pytanie? Chętnie na nie odpowiemy', 'sage') ?></p></div>
  <span class="material-icons icon phone">call</span>
</a>
<a href="#top" id="btn-top"></a>
