@php
$current_lang = pll_default_language();
@endphp
<header class="topbar">
  <div class="container">
    <div class="row">
      <div class="topbar__brand col-xl-3 col-md-12">
        <a class="topbar__logo" href="{{ pll_home_url(  $lang = '' ) }}">
          <img class="img-fluid logo" src="@asset('images/logo.png')" alt="{{ the_field('alt_to_logo', 'option') }}" />
          <span class="logo-text">Autoryzowany Serwis <br/> Volvo Trucks</span>
        </a>
      </div>
      <div class="topbar__additional col-lg-9 col-md-12">
        <img class="img-fluid logo" src="@asset('images/omega-trucks.svg')" alt="{{ the_field('alt_to_omega', 'option') }}"/>
        <a class="btn btn--header" href="tel:{{ str_replace(' ','',get_field('header_phone_number', 'options')) }}">
          <span><?php _e('Serwis Volvo 24/7', 'sage'); ?></span> +48 {{ get_field('header_phone_number', 'options') }}
        </a>
        {{ get_search_form() }}
        <span class="search-icon"></span>
      </div>
      <div id='hamburger' class='hamburger header__hamburger'><span class='hamburger__span'></span></div>
    </div>
  </div>
</header>
