<nav class="navbar">
  <div class="container">
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu([
      'theme_location' => 'primary_navigation',
      'menu_class' => 'navbar-nav ml-auto navbarMenu',
      'menu_id' => '',
      'walker' => new \App\WP_Bootstrap_Navwalker(),
      ]) !!}
    @endif
  </div>
</nav>
