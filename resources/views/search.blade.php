@extends('layouts.app')

@section('content-full')
<section class="search-items">
  <div class="container">
    <div class="row">
      @if (!have_posts())
        <div class="col-12">
          <p class="search-items__no-items">Brak wyników wyszukiwania, spróbuj ponownie lub <a href="/">wróć na stronę główną</a>.</p>
        </div>
        {!! get_search_form(false) !!}
      @endif

      @while(have_posts()) @php the_post() @endphp
        <div class="col-12">
          <h1 class="search-items__title">Wyniki wyszukiwania</h1>
        </div>
        @include('partials.content-search')
      @endwhile

      {!! get_the_posts_navigation() !!}
    </div>
  </div>
</section>
@endsection
