{{--
  Template Name: O nas
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('about-us') )
    @while ( have_rows('about-us') ) @php the_row(); @endphp
      @if( get_row_layout() === 'hero' )
        @include('modules.hero-contracts')

      @elseif( get_row_layout() === 'aboutText_image' )
        @include('modules.aboutText_image')

      @elseif( get_row_layout() === 'box_leftImage_text' )
        @include('modules.leftImage_text')

      @elseif( get_row_layout() === 'blocks-icon' )
        @include('modules.blocks-icon')

      @elseif( get_row_layout() === 'form' )
        @include('modules.page-form')

      @endif
    @endwhile
  @endif
@endsection

