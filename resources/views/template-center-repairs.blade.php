{{--
  Template Name: Centrum napraw powypadkowych
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('center-repairs') )
    @while ( have_rows('center-repairs') ) @php the_row(); @endphp
    @if( get_row_layout() === 'hero' )
      @include('modules.hero-contracts')

    @elseif( get_row_layout() === 'box_leftImage_text' )
      @include('modules.leftImage_text')

    @elseif( get_row_layout() === 'insurance' )
      @include('modules.insurance')

    @elseif( get_row_layout() === 'form' )
      @include('modules.page-form')

    @endif
    @endwhile
  @endif

@endsection
