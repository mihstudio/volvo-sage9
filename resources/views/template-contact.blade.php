{{--
  Template Name: Kontakt
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('contact') )
    @while ( have_rows('contact') ) @php the_row(); @endphp

    @if( get_row_layout() === 'hero' )
      @include('modules.hero-custom-offer')

    @endif
    @endwhile
  @endif
  <section class="contact-map">
    <div class="container">
      <div class="row">
        <div class="col-12">
          @php
            $location = get_field('map', 'option');
          @endphp
          @if($location)
            <div class="contact-map__content">
              <div class="acf-map footer__map" data-zoom="16">
                <div class="marker" data-lat="{{ $location['lat'] }}" data-lng="{{ $location['lng'] }}"></div>
              </div>
              <div class="contact-map__info">
                <div class="contact-map__info-bg">
                  <p class="footer__address">
                    <strong>OMEGA Truck Center sp. z o.o.</strong><br/>
                    {{ get_field('footer_address', 'option') }}
                  </p>
                  <p class="footer__hours">
                    <strong><?php _e( 'Godziny otwarcia', 'sage' ) ?></strong><br/>
                    {{ get_field('footer_hours', 'option') }}
                  </p>
                </div>
                <div class="contact-map__info-bg">
                  <p class="footer__hours">
                    <strong><?php _e( 'Volvo Action Serwis 24/7', 'sage' ) ?></strong><br/>
                    <a href="tel:{{ str_replace(' ','',get_field('fixed-icon_number', 'options')) }}">
                      +48 {{ get_field('fixed-icon_number', 'options') }}
                    </a>
                  </p>
                </div>
                <div class="contact-map__info-bg">
                  <p class="footer__hours">
                    <strong><?php _e( 'Mobilny serwis opon', 'sage' ) ?></strong><br/>
                    <a href="tel:{{ str_replace(' ','',get_field('phone_mobile_tires', 'options')) }}">
                      +48 {{ get_field('phone_mobile_tires', 'options') }}
                    </a>
                  </p>
                </div>
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
  </section>

  <section class="contact-info">
    <div class="container">
      <div class="row">
        <div class="col-12 mb-5 pb-5">
          <h2 class="title-section centerLine">
            <?php _e( 'Nasze numery kontaktowe', 'sage' ); ?>
          </h2>
        </div>
        @if( have_rows('contact_info') )
          @while( have_rows('contact_info') ) @php the_row() @endphp
          @php
            $name_service = get_sub_field('name_service');
          @endphp
          <div class="col-12">
            <h3 class="contact__service">{{ $name_service }}</h3>
          </div>
          @if( have_rows('contacts') )
            @while( have_rows('contacts') ) @php the_row() @endphp
            @php
              $name = get_sub_field('name');
              $info = get_sub_field('info');
              $phone_home = get_sub_field('phone_home');
              $mobile = get_sub_field('mobile');
              $email = get_sub_field('email');
            @endphp
            <div class="col-xl-4 col-md-6 mb-5">
              <div class="contact__content">
                <div class="contact__item">
                  <h4 class="contact__name">
                    {{ $name }}
                  </h4>
                  <div class="contact__department">
                    {{ $info }}
                  </div>
                  @if ($mobile)
                    <a class="contact__icon"
                       href="tel:{{ str_replace(' ','',$mobile) }}">
                      <span class="material-icons">phone_iphone </span> {{ $mobile }}
                    </a>
                  @endif
                  @if ($phone_home)
                    <a class="contact__icon"
                       href="tel:{{ str_replace(' ','',$phone_home) }}">
                      <span class="material-icons">call</span> {{ $phone_home }}
                    </a>
                  @endif
                  @if($email)
                    <a class="contact__icon" href="mailto:{{ $email }}">
                      <span class="material-icons">email</span> {{ $email }}
                    </a>
                  @endif
                </div>
              </div>
            </div>
            @endwhile
          @endif
          @endwhile
        @endif
      </div>
    </div>
  </section>

  @if( have_rows('contact') )
    @while ( have_rows('contact') ) @php the_row(); @endphp

    @if( get_row_layout() === 'form' )
      @include('modules.page-form')

    @endif
    @endwhile
  @endif
@endsection

