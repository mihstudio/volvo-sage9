{{--
  Template Name: Kontrakty
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('sections') )
    @while ( have_rows('sections') ) @php the_row(); @endphp
      @if( get_row_layout() === 'hero' )
        @include('modules.hero-contracts')

      @elseif( get_row_layout() === 'box_leftImage_text' )
        @include('modules.leftImage_text')

      @elseif( get_row_layout() === 'choose_plan' )
        @include('modules.choose-plan')

      @elseif( get_row_layout() === 'form' )
        @include('modules.page-form')

      @endif
    @endwhile
  @endif

@endsection
