{{--
  Template Name: Ciekawostki
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('blog') )
    @while ( have_rows('blog') ) @php the_row(); @endphp
      @if( get_row_layout() === 'hero' )
        @include('modules.hero-contracts')

      @elseif( get_row_layout() === 'curiosities' )
        @include('modules.curiosities')

      @endif
    @endwhile
  @endif
@endsection

