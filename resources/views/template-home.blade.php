{{--
  Template Name: Strona główna
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('homepage') )
    @while ( have_rows('homepage') ) @php the_row(); @endphp
      @if( get_row_layout() === 'hero_home' )
        @include('modules.hero-home')

      @elseif( get_row_layout() === 'offer' )
        @include('modules.home-offer')

      @elseif( get_row_layout() === 'authorizations' )
        @include('modules.home-authorizations')

      @elseif( get_row_layout() === 'blog' )
        @include('modules.home-blog')

      @elseif( get_row_layout() === 'form' )
        @include('modules.page-form')

      @elseif( get_row_layout() === 'newsletter' )
        @include('modules.newsletter')

      @elseif( get_row_layout() === 'box_leftImage_text' )
        @include('modules.leftImage_text')

      @elseif( get_row_layout() === 'box_packages' )
        @include('modules.packages')

      @endif
    @endwhile
  @endif
@endsection

