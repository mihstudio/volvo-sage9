{{--
  Template Name: Okręgowa stacja kontroli pojazdów
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('inspection-station') )
    @while ( have_rows('inspection-station') ) @php the_row(); @endphp
    @if( get_row_layout() === 'hero' )
      @include('modules.hero-contracts')

    @elseif( get_row_layout() === 'box_leftImage_text' )
      @include('modules.leftImage_text')

    @elseif( get_row_layout() === 'box_leftText_image' )
      @include('modules.leftText_image')

    @elseif( get_row_layout() === 'box_leftText_list' )
      @include('modules.leftText_list')

    @elseif( get_row_layout() === 'form' )
      @include('modules.page-form')

    @endif
    @endwhile
  @endif

@endsection
