{{--
  Template Name: Podstrony Ofertowe
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('sections') )
    @while ( have_rows('sections') ) @php the_row(); @endphp

    @if( get_row_layout() === 'hero' )
      @include('modules.hero-custom-offer')

    @elseif( get_row_layout() === 'hero_title' )
      @include('modules.hero-contracts')

    @elseif( get_row_layout() === 'blocks-icon' )
      @include('modules.blocks-icon')

    @elseif( get_row_layout() === 'blocks-icon_column' )
      @include('modules.blocks-icon_column')

    @elseif( get_row_layout() === 'box_leftImage_text' )
      @include('modules.leftImage_text')

    @elseif( get_row_layout() === 'box_leftText_image' )
      @include('modules.leftText_image')

    @elseif( get_row_layout() === 'center_offer' )
      @include('modules.center_offer')

    @elseif( get_row_layout() === 'center_btn' )
      @include('modules.center_withBtn')

    @elseif( get_row_layout() === 'form' )
      @include('modules.page-form')

    @elseif( get_row_layout() === 'authorizations' )
      @include('modules.home-authorizations')

    @elseif( get_row_layout() === 'career' )
      @include('modules.career')

    @elseif( get_row_layout() === 'promo_box' )
      @include('modules.promo')

    @endif
    @endwhile
  @endif

@endsection

