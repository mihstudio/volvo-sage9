{{--
  Template Name: Oferta
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('sections') )
    @while ( have_rows('sections') ) @php the_row(); @endphp
    @if( get_row_layout() === 'hero' )
      @include('modules.hero-offer')

    @elseif( get_row_layout() === 'offer_image' )
      @include('modules.offer.offer-image')

    @elseif( get_row_layout() === 'service_gum' )
      @include('modules.offer.service-gum')

      <section class="different-offer">
        <div class="container">
          <div class="row">
            @elseif( get_row_layout() === 'different_offer' )
              @include('modules.offer.different_offer')

            @elseif( get_row_layout() === 'different_offer_sec' )
              @include('modules.offer.different_offer')
          </div>
        </div>
      </section>

    @elseif( get_row_layout() === 'only_list' )
      @include('modules.offer.only_list')

    @elseif( get_row_layout() === 'form' )
      @include('modules.page-form')

    @endif
    @endwhile
  @endif

@endsection


