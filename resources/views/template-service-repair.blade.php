{{--
  Template Name: Serwis samochodów ciężarowych
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('service-repair') )
    @while ( have_rows('service-repair') ) @php the_row(); @endphp
    @if( get_row_layout() === 'hero' )
      @include('modules.hero-contracts')

    @elseif( get_row_layout() === 'box_leftImage_text' )
      @include('modules.leftImage_text')

    @elseif( get_row_layout() === 'box_leftText_image' )
      @include('modules.leftText_image')

    @elseif( get_row_layout() === 'form' )
      @include('modules.page-form')

    @endif
    @endwhile
  @endif

@endsection
