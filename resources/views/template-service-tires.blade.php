{{--
  Template Name: Serwis opon
--}}

@extends('layouts.app')

@section('content-full')
  @if( have_rows('service-tires') )
    @while ( have_rows('service-tires') ) @php the_row(); @endphp
    @if( get_row_layout() === 'hero' )
      @include('modules.hero-contracts')

    @elseif( get_row_layout() === 'box_leftImage_text' )
      @include('modules.leftImage_text')

    @elseif( get_row_layout() === 'box_leftVideo_text' )
      @include('modules.leftVideo_text')

    @elseif( get_row_layout() === 'box_leftText_image' )
      @include('modules.leftText_image')

    @elseif( get_row_layout() === 'blocks-icon' )
      @include('modules.blocks-icon')

    @elseif( get_row_layout() === 'counters' )
      @include('modules.counters')

    @elseif( get_row_layout() === 'form' )
      @include('modules.page-form')

    @endif
    @endwhile
  @endif

@endsection
